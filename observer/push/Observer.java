package observer.push;

public interface Observer<Template> {

	public void update(Template pushedData);

}