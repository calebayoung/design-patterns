package observer.push;

import java.util.ArrayList;
import java.util.List;

public class Subject<Template> {

	private List<Observer<Template>> observers = new ArrayList<>();

	public void register(Observer<Template> newObserver) {
		observers.add(newObserver);
	}

	protected void notifyObservers(Template pushedData) {
		for (Observer<Template> observer : observers) {
			observer.update(pushedData);
		}
	}

	public void remove(Observer<Template> observer) {
		observers.remove(observer);
	}

	public void clear() {
		observers = new ArrayList<>();
	}

}