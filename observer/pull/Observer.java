package observer.pull;

public interface Observer {

	// The concrete Observer will take a reference to the concrete Subject in the constructor

	public void update();

}