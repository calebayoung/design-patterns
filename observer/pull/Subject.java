package observer.pull;

import java.util.List;
import java.util.ArrayList;

public class Subject {

	private List<Observer> observers = new ArrayList<>();

	public void register (Observer newObserver) {
		observers.add(newObserver);
	}

	protected void notifyObservers() {
		for (Observer observer : observers) {
			observer.update();
		}
	}

	public void remove(Observer observer) {
		observers.remove(observer);
	}

	public void clear() {
		observers = new ArrayList<>();
	}

}